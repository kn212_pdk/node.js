//-------- Task 1 --------
// console.log("Завдання №1");
// const fs = require("fs");
// const numb = fs.readFileSync("input.txt", "utf-8");
// const a = Math.floor(numb / 10);
// const b = numb % 10;
// fs.writeFileSync("output.txt", `${a} ${b}`);

//=========================================
// const fs = require("fs");
// const n = fs.readFileSync("input.txt", "utf-8");
// let a = n % 10;
// let b = Math.floor(n / 100);
// let c = Math.floor((n / 10) % 10);
// let res = a * 100 + c * 10 + b;
// fs.writeFileSync("output.txt", `${res}`);
// ==================-----==========================
// const fs = require('fs');
// const numb = fs.readFileSync('input.txt', 'utf-8');
// const [n, m] = numb.split(' ').map(Number);
// let res;
// if (m > n) {
//   res = m - n + 1;
// } else {
//   res = 1;
// }
// fs.writeFileSync("output.txt", `${res}`);

//==============================================
// const fs = require('fs');
// const data = fs.readFileSync('input.txt', 'utf-8');
// const mass = data.trim().split('\n');
// const n = Number(mass[0]);
// const arr = mass[1].split(' ').map(Number);
// const last = arr[n - 1];
// for (let i = n - 1; i > 0; i--) {
//   arr[i] = arr[i - 1];
// }
// arr[0] = last;
// fs.writeFileSync("output.txt", `${arr.join(' ')}`);
//===================================================

// const fs = require('fs');
// const n = fs.readFileSync('input.txt', 'utf-8');
// let count = 0;
// if(n === 2) {
//   count = 0;
// } else {
//   for(let i = 2; i <= Math.sqrt(n); i++) {
//     if(n % i === 0) {
//       count += 2;
//       if(i * i === n) {
//         count--;
//       }
//     }
//   }
//   if(Number.isInteger(Math.sqrt(n))) {
//     count--;
//   }
// }
// fs.writeFileSync("output.txt", `${count}`);
//=============================================
// const fs = require('fs');
// const input = fs.readFileSync('input.txt', 'utf8').split('\n');
// const ages = input[1].split(' ').map(Number);
// const count = Array(1001).fill(0);
// for (let i = 0; i < ages.length; i++) {
//   count[ages[i]]++;
// }
// let maxAge = 0;
// let maxCount = 0;
// for (let i = 0; i < count.length; i++) {
//   if (count[i] > maxCount) {
//     maxAge = i;
//     maxCount = count[i];
//   }
// }
// fs.writeFileSync("output.txt", `${maxAge}`);
//=============================================
// const fs = require('fs');
// const input = fs.readFileSync('input.txt', 'utf8').split('\n');
// const [N, K] = input[0].split(' ').map(Number);
// const connections = input[1].split(' ').map(Number);
// let connected = 0;
// for (let i = 0; i < K; i++) {
//   connected += connections[i];
// }
// const result = N > connected ? N - connected : 0;
// fs.writeFileSync("output.txt", `${result}`);
//=================================================
const fs = require('fs');
const fileContents = fs.readFileSync('input.txt', 'utf-8');
const values = fileContents.trim().split(' ');
const n = parseInt(values[0]);
const m = parseInt(values[1]);
const x = parseInt(values[2]);
const y = parseInt(values[3]);
const matrix = new Array(n);
for (let i = 0; i < n; i++) {
  matrix[i] = new Array(m);
}
let counter = 1;
for (let i = 0; i < n; i++) {
  if (i % 2 === 0) {
    for (let j = 0; j < m; j++) {
      matrix[i][j] = counter++;
    }
  } else {
    for (let j = m - 1; j >= 0; j--) {
      matrix[i][j] = counter++;
    }
  }
}
fs.writeFileSync("output.txt", `${matrix[x - 1][y - 1]}`);


























// //-------- Task 2 --------
// console.log("Завдання №7336");
// const a2 = 1; // в гривнях
// const b2 = 25; // в копійках
// const n2 = 2; // кількість пиріжків
// const costKop = a2 * 100 * n2 + b2 * n2; 
// const costHryv = Math.floor(costKop / 100); 
// const totalCostKop = costKop % 100;
// console.log(`${costHryv} ${totalCostKop}`);

// // -------- Task 3 --------
// console.log("Завдання №935");
// const number3 = 356;
// const num1 = Math.floor(Math.abs(number3) / 100);
// const num2 = Math.floor(Math.abs(number3) / 10) % 10;
// const num3 = Math.abs(number3) % 10;
// console.log(num1);
// console.log(num2);
// console.log(num3);

// //-------- Task 4 --------
// console.log("Завдання №85");
// function getNumbMatrix(n3, i, j) {
//     let matrix = Array.from(Array(n3), () => new Array(n3));
//     let num = 1; 
//     let top = 0;
//     let bottom = n3 - 1;
//     let left = 0;
//     let right = n3 - 1;
//     while (top <= bottom && left <= right) {
//       for (let k = left; k <= right; k++) {
//         matrix[top][k] = num++;
//       }
//       top++;
//       for (let k = top; k <= bottom; k++) {
//         matrix[k][right] = num++;
//       }
//       right--;
//       for (let k = right; k >= left; k--) {
//         matrix[bottom][k] = num++;
//       }
//       bottom--;
//       for (let k = bottom; k >= top; k--) {
//         matrix[k][left] = num++;
//       }
//       left++;
//     }
//     return matrix[i - 1][j - 1];
//   }
//   const n4 = 5; // Розмір квадратної матриці
//   const i = 4; // Рядок
//   const j = 2; // Стовбець
//   const number4 = getNumbMatrix(n4, i, j);
//   console.log(number4);
  

// //-------- Task 5 --------
// console.log("Завдання №916");
// function findNumber(a, b, c, d) {
//     let numbers = new Set();
//     for (let i = a; i <= b; i++) {
//       for (let j = c; j <= d; j++) {
//         let number = i * j;
//         numbers.add(number);
//       }
//     }
//     return numbers.size;
//   }
//   const a5 = 1; // Початок діапазону для змінної i
//   const b5 = 10; // Кінець діапазону для змінної i
//   const c = 1; // Початок діапазону для змінної j
//   const d = 10; // Кінець діапазону для змінної j
//   const count = findNumber(a5, b5, c, d);
//   console.log(count);


// //-------- Task 6 --------
// console.log("Завдання №2666");
// const n6 = 3;
// const arr = [];
// for (let i = 0; i < n6; i++) {
//   arr[i] = [];
//   for (let j = 0; j < n6; j++) {
//     if (i + j === n6 - 1) {
//       arr[i][j] = 0;
//     } else if (i + j > n6 - 1) {
//       arr[i][j] = 1;
//     } else {
//       arr[i][j] = 2;
//     }
//   }
// }
// arr.forEach(element => {
//     console.log(element);
// });
  