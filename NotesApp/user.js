const { createDiffieHellmanGroup } = require("crypto");
const fs = require("fs")

function add(language) {
    //Прочитати файл user.json
    let userJSON =  fs.readFileSync("user.json", "utf-8")
    //Перетворити дані з файлу в JS-обєкт
    let user = JSON.parse(userJSON);

    //Отримати масив мов і додати новий елемент
    user.languages.push(language)

    //Перетворити JS-обєкт в JSON
    //Записати новий JSON у файл user.json
    fs.writeFileSync("user.json", JSON.stringify(user))
    return (console.log("Мову додано!"));
}

function remove(title) {
    //Прочитати файл user.json
    let userJSON =  fs.readFileSync("user.json", "utf-8")
    //Перетворити дані з файлу в JS-обєкт
    let user = JSON.parse(userJSON);
    for (let i = 0; i < user.languages.length; i++) {
        if (title.title === user.languages[i].title) {
            user.languages.splice(i, 1);
            fs.writeFileSync("user.json", JSON.stringify(user))
            return (console.log("Ви видалили мову!"));
        }
    }
    return (console.log("У файлі нема такої мови!"));
}

function list() {
    //прочитати файл
    let userJson = fs.readFileSync("user.json", "utf-8")
    //перетворити дані з файлу в Js-об'єкт
    let user = JSON.parse(userJson);
    console.log(user.languages)
}

function read(title) {
    //Прочитати файл user.json
    let userJSON = fs.readFileSync("user.json", "utf-8")
    //Перетворити дані з файлу в JS-обєкт
    let user = JSON.parse(userJSON);
    for (let i = 0; i < user.languages.length; i++) {
        if (title.title === user.languages[i].title) {
            return (console.log(user.languages[i]));
        }
    }
    return (console.log("У файлі нема такої мови!"));
}

module.exports = {add, remove, list, read}