require("dotenv").config();
const express = require('express')
const app = express()

const User = require("./models/user")
const Task = require("./models/task")

const userRouter = require("./routers/user")
app.use(express.json())
app.use('/users', userRouter)

const taskRouter = require("./routers/task")
app.use(express.json())
app.use('/tasks', taskRouter)

const port = process.env.PORT;
require("./db/mongoose")

// let url = process.env.MONGO_URL;
// const mongoose = require('mongoose');
// mongoose.connect(url);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
