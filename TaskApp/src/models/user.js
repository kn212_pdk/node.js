const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName:{
        type: String,
        required: true,
        trim: true
    },
    age: {
        type: Number,
        default: 0,
        validate(value) {
            if (value < 0) {
                throw new Error("Невірно вказаний вік!")
            }
        }
    },
    password: {
        type: String,
        required: true,
        minLength: 7,
        trim: true,
        validate(value) {
            if (validator.contains(value, "password")) {
                throw new Error("Password cannot contain the word \"password\"")
            }
        }
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error("Email is invalid")
            }
        }
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]

});
//,{toJSON: {virtuals: true}, toObject: {virtuals: true}}

userSchema.pre('save', async function(next)
{
    const user = this;
    if(user.isModified('password'))
        user.password = await bcrypt.hash(user.password, 8);
    next();
});

userSchema.statics.findOneByCredentials = async (email, password) => {
    const user = await User.findOne({email});
    if(!user) {
        throw new Error("Користувача не знайдено!");
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if(!isMatch){
        throw new Error("Невірний пароль!");
    }
    return user;
};

userSchema.methods.generateAuthToken = async function() {
    const user = this;
    const token = jwt.sign({_id: user._id.toString()}, 'askldfj');

    user.tokens = user.tokens.concat({token});

    await user.save();
    return token;
}

userSchema.virtual('tasks',{
    ref: "Task",
    localField: '_id',
    foreignField: 'owner'
})

userSchema.methods.toJSON = function () {
    const userObject = this.toObject()
    delete userObject.password
    delete userObject.tokens
    return userObject
}

const User = mongoose.model("User", userSchema);
module.exports = User
