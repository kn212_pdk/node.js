const express = require('express');
const router = express.Router();
const Task = require('./../models/task');
const auth = require("./../middleware/auth");

router.use(auth);

// GET /tasks
router.get('/', async (req, res) => {
    try {
        const tasks = await Task.find({owner: req.user.id});
        res.json(tasks);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// PATCH /tasks/:id
router.patch('/:id', getTask, async (req, res) => {


    if (req.body.title != null) {
        res.task.title = req.body.title;
    }

    if (req.body.description != null) {
        res.task.description = req.body.description;
    }

    if (req.body.completed != null) {
        res.task.completed = req.body.completed;
    }

    try {
        const updatedTask = await res.task.save();
        res.json(updatedTask);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// GET /tasks/:id
router.get('/:id', getTask, (req, res) => {
    res.json(res.task);
});

// POST /tasks
router.post('/', async (req, res) => {
    const task = new Task({
        title: req.body.title,
        description: req.body.description,
        completed: req.body.completed,
        owner: req.user.id
    });

    try {
        const newTask = await task.save();
        res.status(201).json(newTask);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});



// DELETE /tasks/:id
router.delete('/:id', getTask, async (req, res) => {
    try {
        await Task.findByIdAndDelete({_id: req.params.id, owner: req.user.id});
        res.json({ message: 'Task was deleted' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// DELETE /tasks
router.delete('/', async (req, res) => {
    try {
        await Task.deleteMany({owner: req.user.id});
        res.json({ message: 'Deleted all tasks' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

async function getTask(req, res, next) {
    try {
        const task = await Task.findById(req.params.id);

        if (task == null) {
            return res.status(404).json({ message: 'Неможливо знайти задачу' });
        }
        if (task.owner.toString() !== req.user.id) {
            return res.status(403).json({ message: 'У доступі відмовлено.' });
        }
        await task.populate("owner")
        res.task = task;
        next();
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
}
// router.get('/', auth, async (req, res) => {
//     try {
//         const tasks = await Task.find({
//             owner: req.user.id
//         })
//         if (tasks.length === 0) {
//             throw new Error('Not found')
//         }
//         res.status(200).send(tasks)
//         //res.json(tasks);
//     } catch (error) {
//         res.status(404).send(error.message);
//     }
// });

// router.get('/:id', auth, async (req, res) => {
//     try {
//         const task = await Task.find({
//             _id: req.params.id,
//             owner: req.user._id
//         })
//         if (task.length === 0) {
//             throw new Error('Not found!')
//         }
//         res.status(200).send(task)
//     } catch (error) {
//         res.status(404).send(error.message)
//     }
//     //res.json(res.task);
// });


// router.post('/', auth, async (req, res) => {
//     if (!req.body) {
//         res.status(400).send()
//     }

//     const newTask = new Task({
//         ...req.body,
//         owner: req.user.id
//     })

//     try {
//         await newTask.save()
//         res.status(200).send(newTask)

//     } catch (error) {
//         res.status(500).send(error.message)
//     }
//     // const task = new Task({
//     //     title: req.body.title,
//     //     description: req.body.description,
//     //     completed: req.body.completed,
//     //     owner: req.user.id
//     // });

//     // try {
//     //     const newTask = await task.save();
//     //     res.status(201).json(newTask);
//     // } catch (err) {
//     //     res.status(400).json({ message: err.message });
//     // }
// });


// router.patch("/:id", auth, async (req, res) => {
//     if (!req.body) {
//         res.status(400).send()
//     }
//     const filter = {
//         _id: req.params.id,
//         owner: req.user._id
//     }
//     const update = req.body
//     try {
//         const task = await Task.findOneAndUpdate(filter, update)
//         if (task.length === 0) {
//             throw new Error('Not found!')
//         }
//         res.status(200).send(task)
//     } catch (error) {
//         res.status(404).send("Not Found!")
//     }
// })
//     // if (req.body.title != null) {
//     //     res.task.title = req.body.title;
//     // }

//     // if (req.body.description != null) {
//     //     res.task.description = req.body.description;
//     // }

//     // if (req.body.completed != null) {
//     //     res.task.completed = req.body.completed;
//     // }
// //     const filter = {
// //         _id: req.params.id,
// //         owner: req.user._id
// //     }
// //     const update = req.body
// //     try {
// //         const updatedTask = await Task.findOneAndUpdate(filter, update);
// //         //res.status(200).send()
// //         //res.json(update);
// //     } catch (err) {
// //         res.status(400).json({ message: err.message });
// //     }
// // });

// // DELETE /tasks/:id

// router.delete('/:id', auth, async (req, res) => {
//     try {
//         await Task.deleteOne({
//             _id: req.params.id,
//             owner: req.user._id
//         })
//         res.status(200).send()
//     } catch (error) {
//         res.status(403).send(error.message)
//     }
//     // try {
//     //     await Task.findByIdAndDelete(req.params.id);
//     //     res.json({ message: 'Task was deleted!' });
//     // } catch (err) {
//     //     res.status(500).json({ message: err.message });
//     // }
// });


// router.delete('/', auth, async (req, res) => {
//     try {
//         await Task.deleteMany({
//             owner: req.user._id
//         });
//         res.status(200).send()
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
// });

module.exports = router;



// const express = require('express');
// const router = express.Router();
// const Task = require('./../models/task');
// const auth = require("./../middleware/auth");

// // GET /tasks
// router.get('/', auth, async (req, res) => {
//     try {
//         const tasks = await Task.find({
//             owner: req.user.id
//         })
//         if (tasks.length === 0) {
//             throw new Error('Not found')
//         }
//         res.status(200).send(tasks)
//         //res.json(tasks);
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

// // GET /tasks/:id
// router.get('/:id', auth, getTask, async (req, res) => {
//     try {
//         const task = await Task.find({
//             _id: req.params.id,
//             owner: req.user._id
//         })
//         if (task.length === 0) {
//             throw new Error('Not found!')
//         }
//         res.status(200).send(task)
//     } catch (error) {
//         res.status(404).send(error.message)
//     }
//     //res.json(res.task);
// });

// // POST /tasks
// router.post('/', auth, async (req, res) => {
//     const task = new Task({
//         title: req.body.title,
//         description: req.body.description,
//         completed: req.body.completed,
//         owner: req.user.id
//     });

//     try {
//         const newTask = await task.save();
//         res.status(201).json(newTask);
//     } catch (err) {
//         res.status(400).json({ message: err.message });
//     }
// });

// // PATCH /tasks/:id
// router.patch('/:id', auth, getTask, async (req, res) => {
//     if (req.body.title != null) {
//         res.task.title = req.body.title;
//     }

//     if (req.body.description != null) {
//         res.task.description = req.body.description;
//     }

//     if (req.body.completed != null) {
//         res.task.completed = req.body.completed;
//     }

//     try {
//         const updatedTask = await res.task.save();
//         res.json(updatedTask);
//     } catch (err) {
//         res.status(400).json({ message: err.message });
//     }
// });

// // DELETE /tasks/:id
// router.delete('/:id', getTask, auth, async (req, res) => {
//     try {
//         await Task.findByIdAndDelete(req.params.id);
//         res.json({ message: 'Task was deleted!' });
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

// // DELETE /tasks
// router.delete('/', auth, async (req, res) => {
//     try {
//         await Task.deleteMany({
//             owner: req.user._id
//         });
//         res.json({ message: 'Deleted all tasks!' });
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// });

// async function getTask(req, res, next) {
//     try {
//         const task = await Task.findById(req.params.id);

//         if (task == null) {
//             return res.status(404).json({ message: 'Unable to find task...' });
//         }

//         res.task = task;
//         next();
//     } catch (err) {
//         res.status(500).json({ message: err.message });
//     }
// }

// module.exports = router;