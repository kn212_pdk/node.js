const express = require("express")
const router = express.Router()
const User = require("./../models/user.js")
const bcrypt = require("bcrypt");
const auth = require("./../middleware/auth");
const Task = require("../models/task");

router.post("/login", async (req, res) => {
    try{
        const user = await User.findOneByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({user, token, message: "Ви успішно увійшли в систему."});
    } catch (e){
        res.status(400).send(e.toString())
    }
});

// POST /users/logout
router.post("/logout", auth, async (req, res) => {
    try{
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token;
        })
        await req.user.save()
        res.send({ message: "Користувач успішно вийшов з системи." });
    } catch (e){
        res.status(500).send
    }
})

// POST /users/logoutAll
router.post("/logoutAll", auth, async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send({ message: "Всі користувачі успішно вийшли з системи." });
    } catch (e) {
        res.status(500).send();
    }
});


// GET /users
router.get('/', auth, async (req, res) => {
    try{
        const users = await User.find();
        res.json(users)
    } catch (error){
        res.send(error.message);
    }

})

// GET /users/me
router.get('/me', auth, async (req, res) => {
        res.send(req.user)
})


// GET /users/:id
router.get('/:id', auth, getUser, (req, res) => {
    res.json(res.user);
});

// POST /users
router.post('/', async (req, res) => {
    try{
        const user = new User(req.body)
        const token = await user.generateAuthToken();
        await user.save();
        res.json(user);
    } catch (error){
        res.send(error.message);
    }

})

// PATCH /users/:id
// для оновлення тільки певих даних про користувача
router.patch('/:id', getUser, auth,  async (req, res) => {
    if (req.body.firstName != null) {
        res.user.firstName = req.body.firstName;
    }
    if (req.body.lastName != null) {
        res.user.lastName = req.body.lastName;
    }

    if (req.body.email != null) {
        res.user.email = req.body.email;
    }
    if (req.body.password != null) {
        res.user.password = req.body.password;
    }

    try {
        const updatedUser = await res.user.save();
        res.json(updatedUser);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});


// DELETE /users
router.delete('/', auth, async (req, res) => {
    try {
        await User.deleteMany();
        res.json({ message: 'Deleted all users' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// DELETE /users/:id
router.delete('/:id', auth, getUser, async (req, res) => {
    try {
        await User.findByIdAndDelete(req.params.id);
        res.json({ message: 'User was deleted' });
        res.json(user)
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


// middleware, що шукає користувача за id та додає до res
async function getUser(req, res, next) {
    try {
        const user = await User.findById(req.params.id);
        await user.populate("tasks")
        // якщо користувача не знайдено, то повертається помилка
        if (user == null) {
            return res.status(404).json({ message: 'Cannot find user' });
        }

        res.user = user;
        next();
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
}


// // GET /users
// router.get('/', auth, async (req, res) => {
//     try{
//         const users = await User.find();
//         res.status(200).send(users)
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
// })

// router.get("/me", auth, async (req, res) => {
//     try {

//         const user = await User.findById(req.user._id)
//         .populate("tasks").exec()
//         const result = {...user}._doc
//         result.tasks = {...user}.$$populatedVirtuals.tasks
//         if (!user) {
//             throw new Error()
//         }

//         res.status(200).json(result)
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
//     //res.send(req.user);
// })



// // GET /users/:id
// router.get('/:id', auth, async (req, res) => {
//     // res.json(res.user);
//     try {
//         const user = await User.find({
//             _id: req.params.id
//         })
//         res.status(200).send(user)
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
// });

// // POST /users
// router.post('/', async (req, res) => {
//     if (!req.body) {
//         res.status(400).send()
//     }
//     const newUser = new User(req.body)
//     try {
//         await newUser.save()
//         res.status(200).send(newUser)
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
//     // try{
//     //     const user = new User(req.body)
//     //     await user.save();
//     //     res.json(user);
//     // } catch (error){
//     //     res.send(error.message);
//     // }
// })

// // PATCH /users/:id
// router.patch('/:id', auth, async (req, res) => {
//     async function save(Model, filter, update) {
//         try {
//             return await Model.findOneAndUpdate(filter, update)
//         } catch (error) {
//             throw new Error(error.message)
//         }
//     }

//     if (!req.body)
//         res.status(400).send()

//     const update = req.body

//     try {
//         await save(User, {_id: req.params.id}, update)
//         res.status(200).send()
//     } catch (error) {
//         res.status(404).send(error.message)
//     }
//     // if (req.body.firtName != null) {
//     //     res.user.firtName = req.body.firtName;
//     // }
//     // if (req.body.lastName != null) {
//     //     res.user.lastName = req.body.lastName;
//     // }

//     // if (req.body.email != null) {
//     //     res.user.email = req.body.email;
//     // }
//     // if (req.body.password != null) {
//     //     res.user.password = req.body.password;
//     // }

//     // try {
//     //     const updatedUser = await res.user.save();
//     //     res.json(updatedUser);
//     // } catch (err) {
//     //     res.status(400).json({ message: err.message });
//     // }
// });

// // DELETE /users
// router.delete('/', auth, async (req, res) => {
//     try {
//         await User.deleteMany({})
//         res.status(200).send()
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
//     // try {
//     //     await User.deleteMany();
//     //     res.json({ message: 'Deleted all users!' });
//     // } catch (err) {
//     //     res.status(500).json({ message: err.message });
//     // }
// });

// // DELETE /users/:id
// router.delete('/:id', auth, async (req, res) => {
//     try {
//         await User.deleteOne({_id: req.params.id})
//         res.status(200).send()
//     } catch (error) {
//         res.status(404).send(error.message)
//     }
//     // try {
//     //     await User.findByIdAndDelete(req.params.id);
//     //     res.json({ message: 'User deleted!' });
//     // } catch (err) {
//     //     res.status(500).json({ message: err.message });
//     // }
// });

// // async function getUser(req, res, next) {
// //     try {
// //         const user = await User.findById(req.params.id);

// //         if (user == null) {
// //             return res.status(404).json({ message: 'Unable to find user...' });
// //         }
// //         res.user = user;
// //         next();
// //     } catch (err) {
// //         res.status(500).json({ message: err.message });
// //     }
// // }

// //login

// router.post("/login", async (req, res) => {
//     try
//     {
//         const user = await User.findOneByCredentials(req.body.email, req.body.password);
//         const token = await user.generateAuthToken();
//         res.json({user, token});
//     } catch (error)
//     {
//         res.status(400).send(error.message);
//     }
// })

// //logout
// router.post("/logout", auth, async (req, res) => {
//     try {
//         req.user.tokens = req.user.tokens.filter((token) => {
//             return token.token != req.token;
//         })
//         await req.user.save();
//         res.send("Вихід успішний!!");
//     } catch (error) {
//         res.status(500).send(error);
//     }
// })

// //logoutAll
// router.post("/logoutAll", auth, async (req, res) => {
//     try {
//         req.user.tokens = [];
//         await req.user.save();
//         res.send("Вхід успішний!");
//     } catch (error) {
//         res.status(500).send(error.message);
//     }
// })

module.exports = router








