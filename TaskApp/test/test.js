require("dotenv").config()
const mongourl = process.env.MONGO_URL
let url = "http://localhost:3000";

const users = [
    {email: 'user1@gmaicom', password: '123456', firstName: "Jane", lastName: "Smith", age: 42},
    {email: 'user1@gmail.com', password: '1234567', firstName: "Olya", lastName: "OO", age: 30},
    {email: 'user2@gmail.com', password: '1234567', firstName: "Petro", lastName: "PP", age: 22},
];

let usersTokens = []

let TasksID = []


let mongoose = require("mongoose");
mongoose.connect(mongourl);

let User = require('../src/models/user');
let Task = require('../src/models/task');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();
// let should = chai.should();
const expect = require('chai').expect;

chai.use(chaiHttp);

describe('TaskApp', () => {
    before(async function () {
         await User.deleteMany({});
        await Task.deleteMany({});
     });

    describe('#1', () => {
        it('1) Реєстрація користувача User1 з помилкою валідації', function (done) {
            chai.request(url)
                .post('/users')
                .send(users[0])
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    // res.body.should.have.property("_id");
                    done();
                });
        });
        it('2) Реєстрація користувача User1 без помилок', function (done) {
            chai.request(url)
                .post('/users')
                .send(users[1])
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property("_id");
                    done();
                });
        });
        it('3) Реєстрація користувача User2 без помилок', function (done) {
            chai.request(url)
                .post('/users')
                .send(users[2])
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property("_id");
                    done();
                });
        });
        it('4) Вхід під User1 з вірними даними', function (done) {
            chai.request(url)
                .post('/users/login')
                .send({
                    email: users[1].email,
                    password: users[1].password
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property('user');
                    res.body.should.have.property('token');
                    res.body.should.have.property('message').eql('Ви успішно увійшли в систему.');
                    usersTokens[0] = res.body.token; // зберігаємо токен для подальшого використання
                    done();
                });
        });
        it('5) Додавання задачі Task1 до User1', function (done) {
            chai.request(url)
                .post('/task')
                .set('Authorization', `Bearer ${usersTokens[0]}`) // встановлюємо заголовок авторизації
                .send({
                    title: 'Task1',
                    description: 'Зробити дз',
                    completed: false
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(201);
                    res.body.should.have.property('_id');
                    TasksID[0] = res.body._id;
                    done();
                });
        });
        it('6) Додавання задачі Task2 до User1', function (done) {
            chai.request(url)
                .post('/task')
                .set('Authorization', `Bearer ${usersTokens[0]}`) // встановлюємо заголовок авторизації
                .send({
                    title: 'Task2',
                    description: 'Купити яблучний сік',
                    completed: false
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(201);
                    res.body.should.have.property('_id');
                    TasksID[1] = res.body._id;
                    done();
                });
        });
        it('7) Отримання задач користувача User1', function (done) {
            chai.request(url)
                .get('/task')
                .set('Authorization', `Bearer ${usersTokens[0]}`) // встановлюємо заголовок авторизації
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.length.should.be.equal(2)
                    done();
                });
        });
        it('8) Отримуємо задачу Task1 по ідентифікатору User1', function (done) {
            chai.request(url)
                .get(`/task/${TasksID[0]}`)
                .set('Authorization', `Bearer ${usersTokens[0]}`) // встановлюємо заголовок авторизації
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property('title').eql('Task1');
                    res.body.should.have.property('completed').eql(false);
                    done();
                });
        });
        it('9) Вихід з User1 ', function (done) {
            chai.request(url)
                .post('/users/logout')
                .set('Authorization', `Bearer ${usersTokens[0]}`) // встановлюємо заголовок авторизації
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Користувач успішно вийшов з системи.');
                    done();
                });
        });
        it('10) Вхід під User2 з вірними даними', function (done) {
            chai.request(url)
                .post('/users/login')
                .send({
                    email: users[2].email,
                    password: users[2].password
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property('user');
                    res.body.should.have.property('token');
                    usersTokens[1] = res.body.token; // зберігаємо токен для подальшого використання
                    done();
                });
        });
        it('11) Додавання задачі Task3 до User2', function (done) {
            chai.request(url)
                .post('/task')
                .set('Authorization', `Bearer ${usersTokens[1]}`) // встановлюємо заголовок авторизації
                .send({
                    title: 'Task3',
                    description: 'Вигуляти собаку',
                    completed: false
                })
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(201);
                    res.body.should.have.property('_id');
                    TasksID[2] = res.body._id;
                    done();
                });
        });
        it('12) Отримання задач користувача User2', function (done) {
            chai.request(url)
                .get('/task')
                .set('Authorization', `Bearer ${usersTokens[1]}`) // встановлюємо заголовок авторизації
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.length.should.be.equal(1)
                    done();
                });
        });
        it('13) Отримуємо задачу Task1 по ідентифікатору User2', function (done) {
            chai.request(url)
                .get(`/task/${TasksID[0]}`)
                .set('Authorization', `Bearer ${usersTokens[1]}`) // встановлюємо заголовок авторизації для користувача 2
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(403);
                    res.body.should.have.property('message').eql('У доступі відмовлено.');
                    done();
                });
        });
        // it('13/15?. Отримуємо задачу Task1 по ідентифікатору (User2) (404)', function (done) {
        //     const invalidId = new mongoose.Types.ObjectId(); // генеруємо новий випадковий ObjectId
        //
        //     chai.request(url)
        //         .get(`/task/${invalidId}`)
        //         .set('Authorization', `Bearer ${usersTokens[1]}`) // встановлюємо заголовок авторизації
        //         .end((err, res) => {
        //             if (err) {
        //                 return done(err);
        //             }
        //             res.should.have.status(404);
        //             res.body.should.have.property('message').eql('Неможливо знайти задачу');
        //             done();
        //         });
        // });
        it('14) Вихід з User2', function (done) {
            chai.request(url)
                .post('/users/logout')
                .set('Authorization', `Bearer ${usersTokens[1]}`) // встановлюємо заголовок авторизації
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(200);
                    res.body.should.have.property('message').eql('Користувач успішно вийшов з системи.');
                    done();
                });
        });
        it('15) Отримуємо задачу Task1 по ідентифікатору User2', function (done) {
            // const invalidId = new mongoose.Types.ObjectId(); // генеруємо новий випадковий ObjectId

            chai.request(url)
                .get(`/task/${TasksID[0]}`)
                // .set('Authorization', `Bearer ${usersTokens[1]}`) // встановлюємо заголовок авторизації
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    res.should.have.status(401);
                    res.body.should.have.property('error').eql('Please authenticate');
                    done();
                });
        });

    });


});


// require("dotenv").config()
// const mongourl = process.env.MONGO_URL
// let url = "http://localhost:3000";

// const users = [
//     {email: 'jane@gmail.com', password: '123456', firstName: "Jane", lastName: "Smith", age: 42},
// ];

// let mongoose = require("mongoose");
// mongoose.connect(mongourl);

// let User = require('../src/models/user');
// let Task = require('../src/models/task');

// //Require the dev-dependencies
// let chai = require('chai');
// let chaiHttp = require('chai-http');
// let should = chai.should();

// chai.use(chaiHttp);

// describe('TaskApp', () => {
//     before(async function () {
//         await User.deleteMany({});
//         await Task.deleteMany({});
//     });

//     describe('Scenario #1', () => {
//         it('User validation (failure)', function (done) {
//             chai.request(url)
//                 .post('/users')
//                 .send(users[0])
//                 .end((err, res) => {
//                     res.status.should.be.equal(400);
//                     res.body.should.have.property("_id");
//                     done();
//                     console.log(`Status code: ${res.status}, (${res.error.text})`);
//                 });
//         });

//     });

// });
