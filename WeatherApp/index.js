const { request } = require('express');
const e = require('express');
const express = require('express');
const hbs = require("hbs");
const fetch = require("node-fetch");
const bodyParser = require('body-parser');

const app = express();
app.set("view engine", "hbs");
app.use(bodyParser.urlencoded({extended:true}));
const port = 3000;

app.get('/', (req, res) => {
    res.render('main')
});

let key = 'b5018676b6c9e7d01aa7056fd2b9186d';
app.get('/weather(/:city?)', async (req, res) => {
    try{
        let city = req.params.city;
        if(!city){
            city = req.query.city;
        }
        if(!city){
            res.render("400")
            return;
        }
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}&units=metric`;
        let response = await fetch(url);  
        let weather = await response.json();
        res.render(`weather`, {city, weather});
    }
    catch(error){
        console.log(error.message)
    }
    
});

app.post('/', async (req, res) => {
    let city = req.body.cityName;
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}&units=metric`;
    let response = await fetch(url);  
    let weather = await response.json();
    res.render(`weather`, {city, weather});
});

app.listen(port, () => {
    console.log(`http://localhost:${port}`);
})