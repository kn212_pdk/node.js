const lodash = require("lodash");

console.log(lodash.compact([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]));
var array = [1];
console.log(lodash.concat(array, 2, [3], [[4]]));

var users = [
    { 'user': 'barney',  'age': 36},
    { 'user': 'fred',    'age': 40}
  ];
console.log( lodash.find(users, function(res) { return res.age < 40; }));

console.log(lodash.includes([1, 2, 3], 1, 2));

console.log(lodash.chunk(['a', 'b', 'c', 'd'], 2));



